const Joi = require('@hapi/joi');
const express = require('express');
const app = express();

app.use(express.json());

const books = [
    { id: 1, title: 'Madame Bovary', author: 'Gustave Flaubert' },
    { id: 2, title: 'Germinal', author: 'Emile Zola' }, 
    { id: 3, title: 'Lolita', author: 'Vladimir Nabokov'},
]



// HOME
app.get('/', (req, res) => {
    res.send('search a book in http://localhost:3000/api/books/');
});

// GET ALL
app.get('/api/books', (req, res) => {
    res.send(books);
});

// POST To add
app.post('/api/books', (req, res) => {    
        //1. Create a book model
    const book = {
        id: books.length + 1,
        title: req.body.title,
        author: req.body.author,
    };
        //2. Validation 
    if (!req.body.title || req.body.title.length < 3 ) {
        // if invalid => 400 bad request
        return res.status(400).send('title is required and have minimum 3 characters.');
    };
        //2. add new book
    books.push(book);
        //3. => new book
    res.send(book);
});

//GET ONE by id
app.get('/api/books/:id', (req, res) => {
        //1. Get the book by id
    const book = books.find(b => b.id === parseInt(req.params.id));
        //if not existing => 404 not found
    if (!book) return res.status(404).send('This book is not found.'); 
        // => book
    res.send(book);
});

// PUT to edit
app.put('/api/books/:id', (req, res) => {
        //1. get the book by id
    const book = books.find(b => b.id === parseInt(req.params.id));
        //if not existing => 404 not found
    if (!book) return res.status(404).send('This book is not found.');
        //2. validation 
    if (!req.body.title || req.body.title.length < 3 ) {
        //if invalid => 400 bad request
    return res.status(400).send('title is required and have minimum 3 characters.');
    }  
        //3. update book
    book.title = req.body.title;
    book.author = req.body.author;
        // => updated book
    res.send(book);
});


// DELETE
app.delete('/api/books/:id', (req, res) => {
        //1. get the book by id
    const book = books.find(b => b.id === parseInt(req.params.id));
        //if not existing => 404 not found
    if (!book) return res.status(404).send('This book is not found.');
        //2. Delete
    const index = books.indexOf(book);
    books.splice(index, 1);
        // => same book
    res.send(book);
});


// PORT
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listening on port ${port}...`));